package octopus

import (
	"fmt"
	"math/rand"
	"os"
	"path"
	"time"
)

const BaseDir = "/tmp"

type octoConf struct {
	BaseDirectory string
	Output        string
	Headers       []string
	Workers       int
	UserAgent     string
	Media         bool
}

var Conf = &octoConf{
	BaseDirectory: BaseDir,
	Output:        "/tmp/out",
	Headers:       nil,
	Workers:       1,
	UserAgent:     "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36",
	Media:         false,
}

func CreateTempDir(base string) string {
	var dirname = base
	if base == BaseDir {
		dirname = path.Join(base, CreateTempName())
		err := os.Mkdir(dirname, 0755)
		if err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "error creating temp directory, aborting\n")
			os.Exit(1)
		}
	} else if _, err := os.Stat(base); err != nil {
		err = os.Mkdir(base, 0755)
		if err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "error creating temp directory %s, aborting\n", base)
			os.Exit(1)
		}
	}

	return dirname
}

func CreateTempName() string {
	rand.Seed(time.Now().UnixNano())
	var letters = []rune("abcdefghijklmnopqrstuvwxyz")
	ran := make([]rune, 6)
	for i := range ran {
		ran[i] = letters[rand.Intn(len(letters))]
	}
	return string(ran)
}
