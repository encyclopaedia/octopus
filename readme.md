# octopus

`octopus` is a file downloader. Plain and simple.

## Features
1. Parallel Downloads <br>
For Large files, `octopus` downloads multiple chunks by parallelizing download <br>
For an input file containing multiple download links, octopus can download in parallel with the number of worker threads
2. Multimedia downloads <br>
For m3u8 and hls files, `octopus` provides an option to download all media files and merge them after download is complete

## Usage
```
Usage:
  octopus <urls> [flags]

Flags:
  -d, --dir string       directory for downloaded files (default "/tmp")
  -H, --header strings   headers to be included for requests
  -h, --help             help for octopus
  -m, --media            treat the downloaded file as an hls file
  -o, --output string    default output file
  -w, --workers int      number of workers for parallel download (default 1)

```

## Examples

## References

TODO: Documentation pending
