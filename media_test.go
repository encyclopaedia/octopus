package octopus

import (
	"fmt"
	"os"
	"testing"
)

func TestDownloadMedia(t *testing.T) {
	testData := []struct {
		name string
		url  string
		//hdr      []string
		out string
		err bool
	}{
		{"happy path - meta", "https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8", "/tmp/happypath.m4a", false},
		{"happy path - streaming", "https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa_video_1080_4800000.m3u8", "/tmp/happypathstreaming.m4a", false},
		{"non-media", "https://example.com", "/tmp/nonmedia.4pm", true},
	}

	for _, test := range testData {
		Conf.Output = test.out
		Conf.Media = true
		input := test.url

		err := DownloadMedia(input, nil)
		if err != nil {
			if test.err {
				fmt.Printf("test success: expected and received error %s\n", err.Error())
			} else {
				t.Errorf("test error: %s -> %s", test.name, err.Error())
			}
		}

		// cleanup
		e := os.Remove(test.out)
		if e != nil {
			_, _ = fmt.Fprintf(os.Stderr, "test error: while removing merged file %s\n", test.out)
		}
	}
}
