package octopus

import (
	"bytes"
	"fmt"
	"github.com/pkg/errors"
	"io"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"
)

type extType string

var nonMediaLines, _ = regexp.Compile(`^(\s+)?$|^#.*$`)

const (
	// ========= streaming media handler ================
	// streaming media means a master m3u8 has links to other m3u8s
	// This is found when the user has to choose between different resolutions of the media file
	// like 360p, 720p etc
	streaming extType = "streaming"
	meta              = "meta"
	unknown           = "unknown"
)

func DownloadMedia(input string, headers []string) error {
	fmt.Printf("treating %s as a media input\n", input)

	// Step 1: Create temp directory and download the media file into temp dir
	tmpDir := CreateTempDir(BaseDir)
	tmpFile := path.Join(tmpDir, CreateTempName())
	fmt.Printf("creating temp directory %s\n", tmpDir)
	e := DownloadFile(input, tmpFile, headers)
	if e != nil {
		return errors.Wrapf(e, "exception while downloading media file\n")
	}

	// Step 2: Determine the type of media - if it's a streaming one or meta media file
	mediaContent, e := readFileContents(tmpFile)
	if e != nil {
		return errors.Wrapf(e, "error reading downloaded file contents\n")
	}
	mediaType, e := determineMediaType(mediaContent)
	if e != nil {
		return errors.Wrapf(e, "error determining media type\n")
	}

	// Step 3: Based on the media type, determine the downloadable urls
	mediaUrls := make([]string, 0)
	switch mediaType {
	case meta:
		// if meta file, use the last line (the highest resolution) by default
		urls := constructDownloadableUrls(mediaContent, input)
		if len(urls) < 1 {
			return errors.Errorf("no media present in the downloaded file\n")
		}
		mediaFile := path.Join(tmpDir, CreateTempName())
		e = DownloadFile(urls[len(urls)-1], mediaFile, headers)
		if e != nil {
			return errors.Wrapf(e, "exception while downloading media file\n")
		}
		mediaContent, _ = readFileContents(mediaFile)
	case streaming:
	default:
		return errors.Errorf("unknown media format. check for youtself in %s\n", tmpFile)
	}

	// Step 4: Download
	mediaUrls = append(mediaUrls, constructDownloadableUrls(mediaContent, input)...)
	DownloadFiles(mediaUrls, tmpDir, headers, Conf.Workers)

	// Step 5: Merge downloaded files
	e = mergeMedia(tmpDir, mediaContent)
	if e != nil {
		return errors.Wrapf(e, "unable to merge media\n")
	}

	// Step 6: Cleanup temp files and directories
	e = os.RemoveAll(tmpDir)
	if e != nil {
		return errors.Wrapf(e, "exception while deleting temp file\n")
	} else {
		fmt.Printf("successfully removed temp dir %s\n", tmpDir)
	}
	return nil
}

func determineMediaType(fileContents [][]byte) (extType, error) {
	streamingMedia := []byte("EXTINF")
	metaMedia := []byte("EXT-X-STREAM")

	for _, line := range fileContents {
		if bytes.Contains(line, streamingMedia) {
			return streaming, nil
		} else if bytes.Contains(line, metaMedia) {
			return meta, nil
		}
	}
	return unknown, errors.Errorf("unknown media type\n")
}

func readFileContents(inFile string) ([][]byte, error) {
	fileContents, err := ioutil.ReadFile(inFile)
	if err != nil {
		return nil, errors.Wrapf(err, "error while opening downloaded media file\n")
	}
	splitLines := bytes.Split(fileContents, []byte("\n"))
	return splitLines, nil
}

func constructDownloadableUrls(fileContents [][]byte, inUrl string) []string {
	urls := make([]string, 0)
	baseUrlTokens := strings.Split(inUrl, "/")
	baseUrl := strings.Replace(inUrl, baseUrlTokens[len(baseUrlTokens)-1], "", 1)
	for _, line := range fileContents {
		if !nonMediaLines.Match(line) {
			//tokens := bytes.Split(line, []byte("/"))
			//name := tokens[len(tokens)-1]
			if len(line) > 4 && string(line[0:4]) == "http" {
				urls = append(urls, string(line))
			} else {
				urls = append(urls, baseUrl+string(line))
			}
		}
	}
	return urls
}

func mergeMedia(tmpDir string, orderFile [][]byte) error {
	out, err := os.OpenFile(Conf.Output, os.O_CREATE|os.O_WRONLY, 0644)
	totalBytes := int64(0)
	if err != nil {
		return errors.Wrapf(err, "unable to create output file %s\n", Conf.Output)
	}
	defer func() { _ = out.Close() }()

	for _, line := range orderFile {
		if nonMediaLines.Match(line) {
			continue
		}
		fNameBase := filepath.Base(string(line))
		fName := path.Join(tmpDir, fNameBase)
		in, err := os.Open(fName)
		if err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "error opening media file %s\n", fName)
		}
		n, err := io.Copy(out, in)
		if err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "error copying file %s\n", fName)
		}
		totalBytes += n
		_ = in.Close()
	}
	fmt.Printf("file ready for use %s - %d bytes\n", out.Name(), totalBytes)
	return nil
}
