module gitlab.com/encyclopaedia/octopus

go 1.15

require (
	github.com/pkg/errors v0.9.1
	github.com/schollz/progressbar/v3 v3.8.5
	github.com/spf13/cobra v1.3.0
)
