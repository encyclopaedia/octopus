package octopus

import (
	"math/rand"
	"sync"
	"testing"
	"time"
)

func TestPbarInit(t *testing.T) {
	//fmt.Printf("thread %02d -> delay=%04d, name=%s\n", t, r, name)
	steps := 100
	var maxDelayMillis int32 = 1000
	pbar := PbarInit(steps)
	// sync group to ensure all tasks completed
	wg := sync.WaitGroup{}
	wg.Add(steps)
	// cap the max number of threads
	maxParallel := 2
	sem := make(chan bool, maxParallel)
	for i := 0; i < steps; i++ {
		sem <- true
		go func() {
			defer wg.Done()
			r := rand.Int31n(maxDelayMillis)
			time.Sleep(time.Duration(r) * time.Millisecond)
			_ = pbar.Add(1)
			<-sem
		}()
	}
}
