package main

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/encyclopaedia/octopus"
	"os"
	"strings"
)

func main() {
	// mandatory variables
	var input = ""

	// root command parser
	var rootCmd = &cobra.Command{
		Use:   "octopus <urls>",
		Short: "downloader for multiple files or large files",
		Long:  `parallel downloader`,
		Args:  cobra.MinimumNArgs(0),
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) > 0 {
				input = strings.Join(args, "\n")
			}
		},
	}

	var baseDir = ""
	rootCmd.Flags().IntVarP(&octopus.Conf.Workers, "workers", "w", 1, "number of workers for parallel download")
	rootCmd.Flags().StringVarP(&baseDir, "dir", "d", octopus.BaseDir, "directory for downloaded files")
	rootCmd.Flags().StringVarP(&octopus.Conf.Output, "output", "o", "", "default output file")
	rootCmd.Flags().StringSliceVarP(&octopus.Conf.Headers, "header", "H", []string{}, "headers to be included for requests")
	rootCmd.Flags().BoolVarP(&octopus.Conf.Media, "media", "m", false, "treat the downloaded file as an hls file")

	// parse input args
	if err := rootCmd.Execute(); err != nil {
		//_, _ = fmt.Fprintf(os.Stderr, "\n%s\n\n%s\n", err.Error(), rootCmd.UsageString())
		//os.Exit(1)
	}

	urls, err := octopus.ParseInput(input)
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "%s\n", err.Error())
		os.Exit(1)
	}
	if len(urls) == 1 {
		url := urls[0]
		if octopus.Conf.Media {
			err = octopus.DownloadMedia(url, octopus.Conf.Headers)
		} else {
			err = octopus.DownloadFile(url, octopus.Conf.Output, octopus.Conf.Headers)
		}
		if err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
	} else {
		// multiple files being downloaded, need basedir to be created
		octopus.Conf.BaseDirectory = octopus.CreateTempDir(baseDir)
		octopus.DownloadFiles(urls, octopus.Conf.BaseDirectory, octopus.Conf.Headers, octopus.Conf.Workers)
	}
}
