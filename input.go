package octopus

import (
	"bufio"
	"github.com/pkg/errors"
	"io/ioutil"
	"os"
	"regexp"
	"strings"
)

// ParseInput reads the input and makes a json out of it
func ParseInput(input string) ([]string, error) {
	urls := make([]string, 0)
	if len(strings.TrimSpace(input)) == 0 {
		r := bufio.NewReader(os.Stdin)
		for {
			line, _, e := r.ReadLine()
			if e != nil {
				break
			}
			urls = append(urls, splitUrls(string(line))...)
		}
	} else if input[0:4] == "http" {
		urls = append(urls, splitUrls(input)...)
	} else if _, e := os.Stat(input); e == nil {
		body, e := ioutil.ReadFile(input)
		if e != nil {
			return nil, errors.Wrapf(e, "error opening the file %s\n", input)
		}
		urls = append(urls, splitUrls(string(body))...)
	} else if e != nil {
		return nil, errors.Wrapf(e, "error stat file %s\n", input)
	} else {
		return nil, errors.New("error: unknown condition")
	}
	return urls, nil
}

var splitRegex = regexp.MustCompile("\\s+")

func splitUrls(input string) []string {
	urls := make([]string, 0)
	for _, i := range splitRegex.Split(input, -1) {
		url := strings.TrimSpace(i)
		if len(url) > 0 {
			urls = append(urls, url)
		}
	}
	return urls
}
