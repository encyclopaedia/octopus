package octopus

import (
	"fmt"
	"github.com/pkg/errors"
	"io"
	"net/http"
	"os"
	"path"
	"strings"
	"sync"
)

var client = &http.Client{
	CheckRedirect: func(req *http.Request, via []*http.Request) error {
		return http.ErrUseLastResponse
	},
}

func Get(url string, headers []string) (*http.Response, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, errors.Wrapf(err, "err while creating http request %s", url)
	}
	for _, h := range headers {
		tokens := strings.SplitN(h, ":", 2)
		req.Header.Add(strings.TrimSpace(tokens[0]), strings.TrimSpace(tokens[1]))
	}
	req.Header.Set("User-Agent", Conf.UserAgent)
	resp, err := client.Do(req)
	if err != nil {
		return nil, errors.Wrapf(err, "err while reading %s", url)
	}
	if resp.StatusCode >= 300 {
		return nil, errors.Errorf("non-success status code %d\n url %s\n", resp.StatusCode, url)
	}
	return resp, nil
}

func DownloadFile(url string, dest string, headers []string) error {
	var name string
	if len(dest) == 0 {
		name = path.Join(BaseDir, extractName(url))
		fmt.Printf("output name not provided, saving as %s\n", name)
	} else {
		name = dest
	}

	resp, err := Get(url, headers)
	if err != nil {
		return errors.Wrapf(err, "error downloading file\n")
	}
	defer func() { _ = resp.Body.Close() }()

	// Create the file
	out, err := os.Create(name)
	if err != nil {
		return errors.Wrapf(err, "error while writing to file %s\n", dest)
	}
	defer func() { _ = out.Close() }()
	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}

func DownloadFiles(urls []string, destDir string, headers []string, nThreads int) {
	var wg sync.WaitGroup
	wg.Add(len(urls))
	// https://stackoverflow.com/questions/25306073/always-have-x-number-of-goroutines-running-at-any-time
	sem := make(chan bool, nThreads)
	// progress bar
	pbar := PbarInit(len(urls))
	for i := 0; i < len(urls); i++ {
		url := urls[i]
		sem <- true
		go func() {
			defer wg.Done()
			toks := strings.Split(url, "/")
			name := toks[len(toks)-1]
			e := DownloadFile(url, path.Join(destDir, name), headers)
			if e != nil {
				_, _ = fmt.Fprintf(os.Stderr, "error while downloading %s, aborting...: \n%s\n", url, e.Error())
				os.Exit(1)
			}
			e = pbar.Add(1)
			if e != nil {
				fmt.Printf("error updating pbar %s\n", e.Error())
			}
			<-sem
		}()
	}
	wg.Wait()
}

func extractName(url string) string {
	tokens := strings.Split(url, "/")
	name := tokens[len(tokens)-1]
	return name
}
