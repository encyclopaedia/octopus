package octopus

import (
	"fmt"
	"os"
	"testing"
	"time"
)

func TestDownloadFile(t *testing.T) {
	testData := []struct {
		name    string
		input   string
		headers []string
		dest    string
	}{
		{"base", "http://ipv4.download.thinkbroadband.com:8080/5MB.zip", []string{}, "/tmp/5MB.zip"},
		{"nil-header", "http://ipv4.download.thinkbroadband.com:8080/5MB.zip", nil, "/tmp/5MB.zip"},
	}
	for _, testcase := range testData {
		e := DownloadFile(testcase.input, testcase.dest, nil)
		if e != nil {
			t.Errorf("%s --> failed, reason=%s", testcase.name, e.Error())
		}
		e = os.Remove(testcase.dest)
		if e != nil {
			t.Errorf("%s --> failed, reason=%s", testcase.name, e.Error())
		}
	}
}

func TestDownloadFiles(t *testing.T) {
	start := time.Now()
	urls := []string{
		"http://ipv4.download.thinkbroadband.com:8080/5MB.zip",
		"http://ipv4.download.thinkbroadband.com:8080/10MB.zip",
		"http://ipv4.download.thinkbroadband.com:8080/20MB.zip",
		"http://ipv4.download.thinkbroadband.com:8080/50MB.zip",
	}
	names := []string{
		"/tmp/5MB.zip",
		"/tmp/10MB.zip",
		"/tmp/20MB.zip",
		"/tmp/50MB.zip",
	}

	DownloadFiles(urls, "/tmp", nil, 2)
	for _, f := range names {
		e := os.Remove(f)
		if e != nil {
			t.Errorf("%s --> failed, unable to remove %s", e.Error(), f)
		}
	}
	fmt.Printf("time taken = %s\n", time.Since(start))
}
